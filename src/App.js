import React, { Component } from "react";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      size: 3,
      currentPlayer: 1,
      isFinished: false,
      winnerIndexes: [],
      score: [[null, null, null], [null, null, null], [null, null, null]]
    };

    this.clickHandler = this.clickHandler.bind(this);
    this.checkWinningState = this.checkWinningState.bind(this);
  }
  clickHandler(event) {
    if (this.state.isFinished) return;
    let field = event.target;
    let score = this.state.score;
    let x = field.dataset["x"];
    let y = field.dataset["y"];

    if (score[x][y] !== null) return;
    score[x][y] = this.state.currentPlayer;
    // event.target.classList.add("field-btn--player-" + this.state.currentPlayer);

    this.setState({
      score: score,
      currentPlayer: (this.state.currentPlayer + 1) % 2
    });
    if (this.checkWinningState().winner) {
      let winningRow = this.checkWinningState();
      this.setState({
        isFinished: true,
        winnerIndexes: winningRow.winnerIndexes
      });
    }
  }

  checkWinningState() {
    let winner = false;
    let winnerIndexes = null;
    let score = this.state.score;
    let size = this.state.size;

    //check horizontal
    this.state.score.some((line, index) => {
      if (
        line.filter(d => d === 0).length === size ||
        line.filter(d => d === 1).length === size
      ) {
        winner = true;
        winnerIndexes = [size * index, size * index + 1, size * index + 2];
        return true;
      } else return false;
    });

    if (winner) return { winner, winnerIndexes };

    // check vertical
    let p1, p2;
    let index = undefined;
    for (let x = 0; x < size; x++) {
      p1 = p2 = 0;
      for (let y = 0; y < size; y++) {
        p1 += score[y][x] === 1 ? 1 : 0;
        p2 += score[y][x] === 0 ? 1 : 0;
      }
      if (p1 === size || p2 === size) {
        winner = true;
        index = size + x;
        winnerIndexes = [index - size, index, index + size];
      }
    }
    if (winner) return { winner, winnerIndexes };

    //check diagonal
    let p1d1 = 0;
    let p1d2 = 0;
    let p2d1 = 0;
    let p2d2 = 0;
    for (let i = 0; i < size; i++) {
      p1d1 += score[i][i] === 1 ? 1 : 0;
      p1d2 += score[i][size - 1 - i] === 1 ? 1 : 0;

      p2d1 += score[i][i] === 0 ? 1 : 0;
      p2d2 += score[i][size - 1 - i] === 0 ? 1 : 0;
      if (p1d1 === size || p2d1 === size) {
        winner = true;
        winnerIndexes = [0, 4, 8];
      } else if (p1d2 === size || p2d2 === size) {
        winner = true;
        winnerIndexes = [2, 4, 6];
      }
    }

    return { winner, winnerIndexes };
  }

  render() {
    let score = this.state.score;
    let fields = [];
    for (let x = 0, i = 0; x < 3; x++) {
      for (let y = 0; y < 3; y++, i++) {
        let classname =
          "field-btn " +
          (this.state.winnerIndexes.indexOf(i) >= 0 ? "winner-btn " : "") +
          (score[x][y] !== null ? "field-btn--player-" + score[x][y] : "");
        fields.push(
          <button
            className={classname}
            data-x={x}
            data-y={y}
            key={x + ":" + y}
            onClick={this.clickHandler}
          />
        );
      }
    }
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">tic-tac-toe</h1>
        </header>
        <div className="field">{fields}</div>
        <h3 className={this.state.isFinished ? "" : "hidden"}>
          We have a winner!
        </h3>
      </div>
    );
  }
}

export default App;
